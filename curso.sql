-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 12-Out-2018 às 07:00
-- Versão do servidor: 5.7.23-0ubuntu0.18.04.1-log
-- PHP Version: 5.6.37-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `curso`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso_pessoa`
--

CREATE TABLE `curso_pessoa` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso_tarefa`
--

CREATE TABLE `curso_tarefa` (
  `id` int(11) NOT NULL,
  `tarefa` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `prazo` date NOT NULL,
  `prioridade` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso_usuario`
--

CREATE TABLE `curso_usuario` (
  `id` int(11) NOT NULL,
  `login` varchar(2552) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `idPessoa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `curso_pessoa`
--
ALTER TABLE `curso_pessoa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curso_tarefa`
--
ALTER TABLE `curso_tarefa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_UsaTarefa` (`idUsuario`);

--
-- Indexes for table `curso_usuario`
--
ALTER TABLE `curso_usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_PesUsario` (`idPessoa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `curso_pessoa`
--
ALTER TABLE `curso_pessoa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `curso_tarefa`
--
ALTER TABLE `curso_tarefa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;
--
-- AUTO_INCREMENT for table `curso_usuario`
--
ALTER TABLE `curso_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `curso_tarefa`
--
ALTER TABLE `curso_tarefa`
  ADD CONSTRAINT `fk_UsaTarefa` FOREIGN KEY (`idUsuario`) REFERENCES `curso_usuario` (`id`);

--
-- Limitadores para a tabela `curso_usuario`
--
ALTER TABLE `curso_usuario`
  ADD CONSTRAINT `fk_PesUsario` FOREIGN KEY (`idPessoa`) REFERENCES `curso_pessoa` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
