<?php 
include_once dirname(__FILE__).'/../../configuracao/Import.php';
Import::dao('AbstractDao.php');
Import::bean('Pessoa.php');
Import::bean('Usuario.php');

// Classe onde terá todos os SQL's referente a manipulação de dados do banco
class LoginDao extends AbstractDao{ 
    
    // Método principal que executart o cadastro de um usuário
    public function cadastrar(Usuario $usuario,Pessoa $pessoa){
        try{
            self::inserirPessoa($pessoa); // Uso da classe inserirPessoa()
            self::inserirUsuario($usuario, $pessoa); // Uso da classe inserirUsuario()
            return true;
        }
        catch (PDOStatement $e){
            return false;
        }
        
    }
    
    public function inserirPessoa(Pessoa $pessoa){
             $sql = parent::getConexao()->action()->prepare("INSERT INTO curso_pessoa (nome,email) VALUES(:nome,:email)");
             $sql->bindParam(':nome', $pessoa->getNome());
             $sql->bindParam(':email', $pessoa->getEmail());
             $sql->execute();
        
    }
    
    
    public function inserirUsuario(Usuario $usuario, Pessoa $pessoa){
        
             $idPessoa = self::selecionarPorNomeEmail($pessoa); // Este método é necessário para fazer a ligação da Entidade Pessoa e Usuario no banco.    
             $sql = parent::getConexao()->action()->prepare("INSERT INTO curso_usuario (senha,login,idPessoa) VALUES(:senha,:login,:idPessoa)");
             $sql->bindParam(':login',$usuario->getLogin());
             $sql->bindParam(':senha',$usuario->getSenha());
             $sql->bindParam(':idPessoa', $idPessoa['id']);
             $sql->execute();
             
    }
    
    public function selecionarPorNomeEmail(Pessoa $pessoa){
        $sql = parent::getConexao()->action()->query("select id from curso_pessoa WHERE nome = '".$pessoa->getNome()."' and email =  '".$pessoa->getEmail()."'");
        $pessoaId =  $sql->fetch(PDO::FETCH_ASSOC);
        return  $pessoaId;
    }
    
    // Método utilizado retornar o Id do usuario
    public function verificarPerfil(Usuario $usuario){
        $sql = parent::getConexao()->action()->query("SELECT id FROM curso_usuario WHERE login = '".$usuario->getLogin()."' AND senha = '".$usuario->getSenha()."'");
        $usuarioId = $sql->fetch(PDO::FETCH_ASSOC);
        return $usuarioId['id'];
    }

}




?>