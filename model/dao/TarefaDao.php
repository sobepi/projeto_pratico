<?php
include_once dirname(__FILE__).'/../../configuracao/Import.php';
Import::dao('AbstractDao.php');
Import::bean('Usuario.php');
Import::bean('Tarefa.php');

class TarefaDao extends AbstractDao{ 
     
    public function cadastrarTarefa(Tarefa $tarefa, $idUsuario){

            $sql = parent::getConexao()->action()->prepare("INSERT INTO curso_tarefa (tarefa,descricao,prazo,prioridade,idUsuario) VALUES(:tarefa,:descricao,:prazo,:prioridade,:idUsuario)");
            $sql->bindParam(':tarefa', $tarefa->getTarefa());
            $sql->bindParam(':descricao',$tarefa->getDescricao());
            $sql->bindParam(':prazo', $tarefa->getPrazo());
            $sql->bindParam(':prioridade', $tarefa->getPrioridade());
            $sql->bindParam(':idUsuario', $idUsuario);
            $sql->execute();
    }
    
    public function buscarTarefaPorIdUsuario($idUsuario){
        $sql = parent::getConexao()->action()->query("SELECT id,tarefa,descricao,prazo,prioridade FROM curso_tarefa WHERE idUsuario = ".$idUsuario);
        $tarefaIds = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $tarefaIds;
    }
    
    public function deletarTarefa($idTarefa){
        $sql = parent::getConexao()->action()->prepare("DELETE FROM curso_tarefa WHERE id = :id");    
        $sql->bindParam(':id', $idTarefa);
        $sql->execute();
    }
    
    public function updateTarefa($idTarefa,Tarefa $tarefa){
        $sql = parent::getConexao()->action()->prepare("UPDATE curso_tarefa SET tarefa = :tarefa WHERE  id = :id");
        $sql->bindParam(':tarefa', $tarefa->getTarefa());
        $sql->bindParam(':id', $idTarefa);
        $sql->execute();
    }
    
    public function updateDescricao($idTarefa,Tarefa $tarefa){
        $sql = parent::getConexao()->action()->prepare("UPDATE curso_tarefa SET descricao = :descricao WHERE  id = :id");
        $sql->bindParam(':descricao', $tarefa->getDescricao());
        $sql->bindParam(':id', $idTarefa);
        $sql->execute();
    }
    
    public function updatePrazo($idTarefa,Tarefa $tarefa){
        $sql = parent::getConexao()->action()->prepare("UPDATE curso_tarefa SET prazo = :prazo WHERE  id = :id");
        $sql->bindParam(':prazo', $tarefa->getPrazo());
        $sql->bindParam(':id', $idTarefa);
        $sql->execute();
    }
    
    public function updatePrioridade($idTarefa,Tarefa $tarefa){
        $sql = parent::getConexao()->action()->prepare("UPDATE curso_tarefa SET prioridade = :prioridade WHERE  id = :id");
        $sql->bindParam(':prioridade', $tarefa->getPrioridade());
        $sql->bindParam(':id', $idTarefa);
        $sql->execute();
    }
    
    public function buscarTarefaPorIdTarefa($idTarefa){
        $sql = parent::getConexao()->action()->query("SELECT tarefa FROM curso_tarefa WHERE id = ".$idTarefa);
        $tarefa = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $tarefa;
    }
}


?>