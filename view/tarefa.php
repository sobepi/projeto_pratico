<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>GERENCIADOR DE TAREFAS</title>
	 <link rel="stylesheet" href="style/css/bootstrap.css">
</head>
<body >
		<!-- O ControllerTarefa irá gerenciar toda a dinâmica por trás as ações feitas pelo usuário na view --> 
		<?php 
      		include_once '../configuracao/Import.php';
  	 	    Import::controller('ControllerTarefa.php');
  		    $controllerTarefa = new ControllerTarefa();
  		    $controllerTarefa->cadastrarTarefa();
  		    $controllerTarefa->redirecionarPaginaEditar();
  		    $controllerTarefa->deletarTarefa();
  		    $controllerTarefa->sair();
  		?>

    	<div class="container">
 	 		<div class="row">
 	 			<div class="col-lg-4 ">
 	 			</div>
    			<div class="col-lg-4 ">
      			<form action="tarefa.php" class="border border-primary py-2 px-2" method="get">
  					<div class="form-group">
  						<p style="text-align:center;">Cadastro de tarefas<p>
    					<label for="tarefa">Tarefa</label>
    						<input type="text" name="tarefa" class="form-control"  placeholder="Insira sua tarefa">
  					</div>
  					<div class="form-group">
    					<label for="email">Descricao</label>
    					<input type="text" class="form-control" name="descricao"  placeholder="Insira sua descrição">
  					</div>
  					 <div class="form-group">
    					<label for="data">Prazo</label>
    					<input type="date" name="prazo" class="form-control">
  					 </div>
  					 <label for="data">Prioriade</label>
  					   <div class="form-check">
          					<input class="form-check-input" type="radio" name="prioridade" id="gridRadios1" value="1" checked>
          						<label class="form-check-label" for="gridRadios1">
            						Baixa
          					</label>
        				</div>
        				<div class="form-check">
          					<input class="form-check-input" type="radio" name="prioridade" id="gridRadios2" value="2">
          					<label class="form-check-label" for="gridRadios2">
            						Média
          					</label>
        				</div>
        				<div class="form-check">
          					<input class="form-check-input" type="radio" name="prioridade" id="gridRadios2" value="3">
          					<label class="form-check-label" for="gridRadios2">
            						Alta
          					</label>
        				</div>
          
  					 <br>
  					<button type="submit" name="cadastrar" value="true" class="btn btn-primary">Cadastrar Tarefa</button>
  					<button type="submit" name="sair" value="true"class="btn btn-danger">Sair</button>
				</form>
      			</div>
      			<div class="col-lg-4 ">
 	 			</div>
    		</div>
  		</div><br>
  		<form action="tarefa.php" method="get">
  		<table class="table">
 			 <thead class="thead-dark">
    		<tr>
     			 <th scope="col">Tarefa</th>
     			 <th scope="col">Descriçao</th>
     			 <th scope="col">Prazo</th>
     			 <th scope="col">Prioridade</th>
     			 <th scope="col">Excluir</th>
     			 <th scope="col">Editar</th>
    			</tr>
  			</thead>
  			<tbody>
  			<?php $controllerTarefa->mostrarTarefas();
             ?>
 			 </tbody>
			</table>
	  </form>
  	
    <script src="style/js/jquery-3.3.1.slim.min.js"></script>
    <script src="style/js/popper.min.js" ></script>
    <script src="style/js/bootstrap.min.js"></script>
    
</body>
</html>
