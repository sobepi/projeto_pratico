<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>GERENCIDADOR DE TAREFAS</title>
<!--     <link rel="stylesheet" href="style/css/style.css" /> -->
	 <link rel="stylesheet" href="style/css/bootstrap.css">
</head>
<body >
        <!-- O ControllerTarefa irá gerenciar toda a dinâmica por trás da edição das informações no banco de dados --> 
		<?php 
      		include_once '../configuracao/Import.php';
  	 	    Import::controller('ControllerTarefa.php');
  		    $controllerTarefa = new ControllerTarefa();
  		    $controllerTarefa->updateTarefa();
  		?>

    	<div class="container">
 	 		<div class="row">
 	 			<div class="col-lg-4 ">
 	 			</div>
    			<div class="col-lg-4 ">
      			<form action="editar.php" class="border border-primary py-2 px-2" method="get">
  					<div class="form-group ">
  						<p style="text-align:center;">Edição<p>
    					<label for="tarefa">Tarefa</label>
    						<input type="text" name="tarefa" class="form-control"  placeholder="Atualize sua tarefa">
  					</div>
  					<div class="form-group">
    					<label for="email">Descricao</label>
    					<input type="text" class="form-control" name="descricao"  placeholder="Atualize sua descricao">
  					</div>
  					 <div class="form-group">
    					<label for="data">Prazo</label>
    					<input type="date" name="prazo" class="form-control">
  					 </div>
  					 <label for="data">Prioriade</label>
  					   <div class="form-check">
          					<input class="form-check-input" type="radio" name="prioridade" id="gridRadios1" value="1" checked>
          						<label class="form-check-label" for="gridRadios1">
            						Baixa
          					</label>
        				</div>
        				<div class="form-check">
          					<input class="form-check-input" type="radio" name="prioridade" id="gridRadios2" value="2">
          					<label class="form-check-label" for="gridRadios2">
            						Média
          					</label>
        				</div>
        				<div class="form-check">
          					<input class="form-check-input" type="radio" name="prioridade" id="gridRadios2" value="3">
          					<label class="form-check-label" for="gridRadios2">
            						Alta
          					</label>
        				</div>
          
  					 <br>
  					<button class="btn btn-primary" name="editarTarefa" value="true">Cadastrar Tarefa</button>
				</form>
      			</div>
      			<div class="col-lg-4 ">
 	 			</div>
    		</div>
  		</div>
<!--   </tbody> -->
<!-- </table> -->
<!-- <div> -->
  	
    <script src="style/js/jquery-3.3.1.slim.min.js"></script>
    <script src="style/js/popper.min.js" ></script>
    <script src="style/js/bootstrap.min.js"></script>
    
</body>
</html>