<?php

class Import{
    
    private static $nomeProjeto = 'projeto_pratico';
    
    public static function configuracao($arquivo){
        $caminho = self::getCaminhoPrincipal()."/configuracao/".$arquivo;
        
        if(self::caminhoExiste($caminho)){
            include_once $caminho;
        }
    }
    
    public static function library($arquivo){
        $caminho = self::getCaminhoPrincipal()."/library/".$arquivo;
        
        if(self::caminhoExiste($caminho)){
            include_once $caminho;
        }
    }
    
    public static function controller($arquivo){
        $caminho = self::getCaminhoPrincipal()."/controller/".$arquivo;
        
        if(self::caminhoExiste($caminho)){
            include_once $caminho;
        }
    }
    
    public static function dao($arquivo){
        $caminho = self::getCaminhoPrincipal()."/model/dao/".$arquivo;
        
        if(self::caminhoExiste($caminho)){
            include_once $caminho;
        }
    }
    
    public static function bean($arquivo){
        $caminho = self::getCaminhoPrincipal()."/model/bean/".$arquivo;
        
        if(self::caminhoExiste($caminho)){
            include_once $caminho;
        }
    }
    
    public static function view($arquivo){
        $caminho = self::getCaminhoPrincipal()."/view/".$arquivo;
        
        if(self::caminhoExiste($caminho)){
            include_once $caminho;
        }
    }

    public static function getCaminhoPrincipal()
    {
        return $_SERVER['DOCUMENT_ROOT']."/".self::$nomeProjeto."/";
    }
    
    public static function caminhoExiste($arquivo){
        if(file_exists($arquivo)){
            return true;
        }
        else{
            return true;
        }
    }
    
}



?>